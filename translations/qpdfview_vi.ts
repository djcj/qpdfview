<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Model::ImageDocument</name>
    <message>
        <location filename="../sources/imagemodel.cpp" line="126"/>
        <source>Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="154"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="155"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="156"/>
        <source>Depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="164"/>
        <location filename="../sources/imagemodel.cpp" line="167"/>
        <location filename="../sources/imagemodel.cpp" line="170"/>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <location filename="../sources/imagemodel.cpp" line="178"/>
        <location filename="../sources/imagemodel.cpp" line="182"/>
        <source>Format</source>
        <translation type="unfinished">Định dạng</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="164"/>
        <source>Monochrome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="167"/>
        <source>Indexed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="170"/>
        <source>32 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <source>32 bits ARGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="178"/>
        <source>16 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="182"/>
        <source>24 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Model::PdfDocument</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <source>Linearized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Name</source>
        <translation type="unfinished">Tên</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Type</source>
        <translation type="unfinished">Thể loại</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Embedded</source>
        <translation type="unfinished">Embedded</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Subset</source>
        <translation type="unfinished">Subset</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>File</source>
        <translation type="unfinished">Tệp</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <location filename="../sources/pdfmodel.cpp" line="935"/>
        <location filename="../sources/pdfmodel.cpp" line="936"/>
        <source>Yes</source>
        <translation type="unfinished">Vâng</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="910"/>
        <source>PDF version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <source>Encrypted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <location filename="../sources/pdfmodel.cpp" line="935"/>
        <location filename="../sources/pdfmodel.cpp" line="936"/>
        <source>No</source>
        <translation type="unfinished">Không</translation>
    </message>
</context>
<context>
    <name>Model::PdfPage</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="599"/>
        <source>Information</source>
        <translation type="unfinished">Thông tin</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="599"/>
        <source>Version 0.20.1 or higher of the Poppler library is required to add or remove annotations.</source>
        <translation type="unfinished">Phiên bản 0.20.1 hoặc cao thơn của thư viện Poppler  để thêm hoặc xóa ghi chú.</translation>
    </message>
</context>
<context>
    <name>Model::PsDocument</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="241"/>
        <source>Title</source>
        <translation type="unfinished">Tiêu đề</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="242"/>
        <source>Created for</source>
        <translation type="unfinished">Tạo ra cho</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="243"/>
        <source>Creator</source>
        <translation type="unfinished">Tác giả</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="244"/>
        <source>Creation date</source>
        <translation type="unfinished">Ngày tạo</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="245"/>
        <source>Format</source>
        <translation type="unfinished">Định dạng</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="246"/>
        <source>Language level</source>
        <translation type="unfinished">Cấp độ ngôn ngữ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/main.cpp" line="152"/>
        <source>An empty instance name is not allowed.</source>
        <translation type="unfinished">Không cho phép tên để trống</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="163"/>
        <source>An empty search text is not allowed.</source>
        <translation type="unfinished">Không cho phép tìm văn bản trống</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="191"/>
        <source>Choose instance</source>
        <translation type="unfinished">Chọn yêu cầu</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="191"/>
        <source>Instance:</source>
        <translation type="unfinished">Yêu cầu:</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="220"/>
        <source>Unknown command-line option &apos;%1&apos;.</source>
        <translation type="unfinished">Không rõ tùy chọn dòng lệnh &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="251"/>
        <source>Using &apos;--instance&apos; requires an instance name.</source>
        <translation type="unfinished">Sử dụng &apos;--instance&apos; bắt buộc tên yêu càu</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="257"/>
        <source>Using &apos;--instance&apos; is not allowed without using &apos;--unique&apos;.</source>
        <translation type="unfinished">Using &apos;--instance&apos; is not allowed without using &apos;--unique&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="263"/>
        <source>An instance name must only contain the characters &quot;[A-Z][a-z][0-9]_&quot; and must not begin with a digit.</source>
        <translation type="unfinished">An instance name must only contain the characters &quot;[A-Z][a-z][0-9]_&quot; and must not begin with a digit.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="269"/>
        <source>Using &apos;--search&apos; requires a search text.</source>
        <translation type="unfinished">Using &apos;--search&apos; requires a search text.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="347"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Không tìm thấy dữ liệu SyncTex cho &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="446"/>
        <source>Could not prepare signal handler.</source>
        <translation type="unfinished">Không thể nhận tín hiệu.</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="856"/>
        <source>Shift</source>
        <translation type="unfinished">Shift</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="857"/>
        <source>Ctrl</source>
        <translation type="unfinished">Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="858"/>
        <source>Alt</source>
        <translation type="unfinished">Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="859"/>
        <source>Shift and Ctrl</source>
        <translation type="unfinished">Shift và Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="860"/>
        <source>Shift and Alt</source>
        <translation type="unfinished">Shift và Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="861"/>
        <source>Ctrl and Alt</source>
        <translation type="unfinished">Ctrl và Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="862"/>
        <source>Right mouse button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="863"/>
        <source>Middle mouse button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="864"/>
        <source>None</source>
        <translation type="unfinished">Trống</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkDialog</name>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="39"/>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="48"/>
        <source>Page:</source>
        <translation type="unfinished">Trang:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="53"/>
        <source>Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="58"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="64"/>
        <source>Modified:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkMenu</name>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="39"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Mở</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="44"/>
        <source>Open in new &amp;tab</source>
        <translation type="unfinished">Mở &amp;trong thẻ mới</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="54"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished">&amp;Bỏ đánh dấu</translation>
    </message>
</context>
<context>
    <name>qpdfview::Database</name>
    <message>
        <location filename="../sources/database.cpp" line="910"/>
        <source>Jump to page %1</source>
        <translation type="unfinished">Đến trang %1</translation>
    </message>
</context>
<context>
    <name>qpdfview::DocumentView</name>
    <message>
        <location filename="../sources/documentview.cpp" line="1006"/>
        <location filename="../sources/documentview.cpp" line="1633"/>
        <source>Information</source>
        <translation type="unfinished">Thông tin</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1006"/>
        <source>The source editor has not been set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1633"/>
        <source>Opening URL is disabled in the settings.</source>
        <translation type="unfinished">Không cho phép mở URL trong thiết đặt</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1673"/>
        <source>Warning</source>
        <translation type="unfinished">Cảnh báo</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1673"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Không tìm thấy dữ liệu SyncTex cho &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2141"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation type="unfinished">Đang in &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2215"/>
        <source>Unlock %1</source>
        <translation type="unfinished">Mở khóa %1</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2215"/>
        <source>Password:</source>
        <translation type="unfinished">Mật khẩu:</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2257"/>
        <source>Page %1</source>
        <translation type="unfinished">Trang %1</translation>
    </message>
</context>
<context>
    <name>qpdfview::FileAttachmentAnnotationWidget</name>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="112"/>
        <source>Save...</source>
        <translation type="unfinished">Lưu...</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="113"/>
        <source>Save and open...</source>
        <translation type="unfinished">Lưu và mở...</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="160"/>
        <source>Save file attachment</source>
        <translation type="unfinished">Lưu tệp đính kèm</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="176"/>
        <location filename="../sources/annotationwidgets.cpp" line="182"/>
        <source>Warning</source>
        <translation type="unfinished">Cảnh báo</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="176"/>
        <source>Could not open file attachment saved to &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể mở tệp đính kèm được lưu vào &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="182"/>
        <source>Could not save file attachment to &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể lưu tệp đính kèm cho &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>qpdfview::FontsDialog</name>
    <message>
        <location filename="../sources/fontsdialog.cpp" line="37"/>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::HelpDialog</name>
    <message>
        <location filename="../sources/helpdialog.cpp" line="40"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="47"/>
        <source>help.html</source>
        <extracomment>Please replace by file name of localized help if available, e.g. &quot;help_fr.html&quot;.</extracomment>
        <translation type="unfinished">help.html</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="62"/>
        <source>Find previous</source>
        <translation type="unfinished">Tìm ngược</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="66"/>
        <source>Find next</source>
        <translation type="unfinished">Tìm tiếp</translation>
    </message>
</context>
<context>
    <name>qpdfview::MainWindow</name>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2957"/>
        <source>Toggle tool bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2958"/>
        <source>Toggle menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="363"/>
        <location filename="../sources/mainwindow.cpp" line="446"/>
        <location filename="../sources/mainwindow.cpp" line="1219"/>
        <location filename="../sources/mainwindow.cpp" line="1236"/>
        <location filename="../sources/mainwindow.cpp" line="1253"/>
        <location filename="../sources/mainwindow.cpp" line="1291"/>
        <location filename="../sources/mainwindow.cpp" line="1434"/>
        <location filename="../sources/mainwindow.cpp" line="2561"/>
        <location filename="../sources/mainwindow.cpp" line="2575"/>
        <source>Warning</source>
        <translation type="unfinished">Cảnh báo</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="363"/>
        <location filename="../sources/mainwindow.cpp" line="446"/>
        <source>Could not open &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể mở &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="683"/>
        <source>Copy file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="684"/>
        <source>Select file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="686"/>
        <source>Close all tabs</source>
        <translation type="unfinished">Đóng tất cả cá thẻ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="687"/>
        <source>Close all tabs but this one</source>
        <translation type="unfinished">Đóng tất cả trừ thẻ này</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="688"/>
        <source>Close all tabs to the left</source>
        <translation type="unfinished">Đóng tất cả thẻ bên trái</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="689"/>
        <source>Close all tabs to the right</source>
        <translation type="unfinished">Đóng tất cả thẻ bên phải</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1164"/>
        <source>Open</source>
        <translation type="unfinished">Mở</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1180"/>
        <source>Open in new tab</source>
        <translation type="unfinished">Mở trong thẻ mới</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1219"/>
        <location filename="../sources/mainwindow.cpp" line="1434"/>
        <source>Could not refresh &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể làm tươi &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1226"/>
        <source>Save copy</source>
        <translation type="unfinished">Lưu bản sao</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1236"/>
        <source>Could not save copy at &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể lưu bản sao tại &apos;%1.&apos;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1243"/>
        <location filename="../sources/mainwindow.cpp" line="2565"/>
        <source>Save as</source>
        <translation type="unfinished">Lưu với tên khác</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1253"/>
        <location filename="../sources/mainwindow.cpp" line="2575"/>
        <source>Could not save as &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể lưu với tên &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1291"/>
        <source>Could not print &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể in &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1327"/>
        <source>Set first page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1327"/>
        <source>Select the first page of the body matter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1340"/>
        <source>Jump to page</source>
        <translation type="unfinished">Đến trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1340"/>
        <source>Page:</source>
        <translation type="unfinished">Trang:</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1741"/>
        <source>Jump to page %1</source>
        <translation type="unfinished">Đến trang %1</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1892"/>
        <source>About qpdfview</source>
        <translation type="unfinished">Về qpdfview</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1892"/>
        <source>&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview is a tabbed document viewer using Qt.&lt;/p&gt;&lt;p&gt;This version includes:&lt;ul&gt;</source>
        <translation type="unfinished">&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview là trình xem tài liệu theo thẻ viết bằng Qt.&lt;/p&gt;&lt;p&gt;Phiên bản này bao gồm:&lt;ul&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1896"/>
        <source>&lt;li&gt;PDF support using Poppler %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1899"/>
        <source>&lt;li&gt;PS support using libspectre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1902"/>
        <source>&lt;li&gt;DjVu support using DjVuLibre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1905"/>
        <source>&lt;li&gt;PDF support using Fitz %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1908"/>
        <source>&lt;li&gt;Printing support using CUPS %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2196"/>
        <source>&amp;Edit bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2561"/>
        <source>The document &apos;%1&apos; has been modified. Do you want to save your changes?</source>
        <translation type="unfinished">Tài liệu &apos;%1&apos; đã bị sửa đổi. Có muốn lưu các thay đổi của bạn?</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2770"/>
        <source>Page width</source>
        <translation type="unfinished">Độ rộng trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2771"/>
        <source>Page size</source>
        <translation type="unfinished">Kích thươc trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2794"/>
        <source>Match &amp;case</source>
        <translation type="unfinished">&amp;Khớp từ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2795"/>
        <source>Whole &amp;words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2796"/>
        <source>Highlight &amp;all</source>
        <translation type="unfinished">Tô &amp;sáng tất cả</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2858"/>
        <source>&amp;Open...</source>
        <translation type="unfinished">&amp;Mở</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2859"/>
        <source>Open in new &amp;tab...</source>
        <translation type="unfinished">Mở &amp;trong thẻ mới</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2860"/>
        <source>Open &amp;copy in new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2862"/>
        <source>&amp;Refresh</source>
        <translation type="unfinished">Là&amp;m tươi</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2863"/>
        <source>&amp;Save copy...</source>
        <translation type="unfinished">&amp;Lưu bản sao</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2864"/>
        <source>Save &amp;as...</source>
        <translation type="unfinished">Lưu &amp;với tên...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2865"/>
        <source>&amp;Print...</source>
        <translation type="unfinished">&amp;In</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2866"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">T&amp;hoát</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2870"/>
        <source>&amp;Previous page</source>
        <translation type="unfinished">T&amp;rang trước</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2871"/>
        <source>&amp;Next page</source>
        <translation type="unfinished">Trang &amp;kế</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2872"/>
        <source>&amp;First page</source>
        <translation type="unfinished">Tr&amp;ang đầu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2873"/>
        <source>&amp;Last page</source>
        <translation type="unfinished">Trang &amp;cuối</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2875"/>
        <source>&amp;Set first page...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2877"/>
        <source>&amp;Jump to page...</source>
        <translation type="unfinished">Đế&amp;n trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2879"/>
        <source>Jump &amp;backward</source>
        <translation type="unfinished">&amp;Lùi</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2880"/>
        <source>Jump for&amp;ward</source>
        <translation type="unfinished">&amp;Tiến</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2882"/>
        <source>&amp;Search...</source>
        <translation type="unfinished">Tìm...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2883"/>
        <source>Find previous</source>
        <translation type="unfinished">Tìm ngược</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2884"/>
        <source>Find next</source>
        <translation type="unfinished">Tìm tiếp</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2885"/>
        <source>Cancel search</source>
        <translation type="unfinished">Hủy tìm kiếm</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2887"/>
        <source>&amp;Copy to clipboard</source>
        <translation type="unfinished">Chếp &amp;vào bộ nhớ đệm</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2888"/>
        <source>&amp;Add annotation</source>
        <translation type="unfinished">Thêm &amp;ghi chú</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2890"/>
        <source>Settings...</source>
        <translation type="unfinished">Thiết lập...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2894"/>
        <source>&amp;Continuous</source>
        <translation type="unfinished">Tiếp nối</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2895"/>
        <source>&amp;Two pages</source>
        <translation type="unfinished">Hai trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2896"/>
        <source>Two pages &amp;with cover page</source>
        <translation type="unfinished">Hai trang với khung</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2897"/>
        <source>&amp;Multiple pages</source>
        <translation type="unfinished">Nhiều trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2899"/>
        <source>Right to left</source>
        <translation type="unfinished">Phải sang trái</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2901"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished">&amp;Phóng to</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2902"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished">&amp;Thu nhỏ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2903"/>
        <source>Original &amp;size</source>
        <translation type="unfinished">&amp;Kích thước gốc</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2905"/>
        <source>Fit to page width</source>
        <translation type="unfinished">Khít theo độ &amp;rộng trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2906"/>
        <source>Fit to page size</source>
        <translation type="unfinished">Khít theo kích &amp;thước trang</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2908"/>
        <source>Rotate &amp;left</source>
        <translation type="unfinished">X&amp;oay qua trái</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2909"/>
        <source>Rotate &amp;right</source>
        <translation type="unfinished">Xoa&amp;y qua phải</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2911"/>
        <source>Invert colors</source>
        <translation type="unfinished">Đảo màu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2912"/>
        <source>Convert to grayscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2913"/>
        <source>Trim margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2915"/>
        <source>Darken with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2916"/>
        <source>Lighten with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2918"/>
        <source>Fonts...</source>
        <translation type="unfinished">Font chữ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2920"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished">Toàn màn &amp;hình</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2921"/>
        <source>&amp;Presentation...</source>
        <translation type="unfinished">Trình chiế&amp;u</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2925"/>
        <source>&amp;Previous tab</source>
        <translation type="unfinished">Thẻ trước</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2926"/>
        <source>&amp;Next tab</source>
        <translation type="unfinished">Thẻ kế</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2928"/>
        <source>&amp;Close tab</source>
        <translation type="unfinished">Đóng thẻ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2929"/>
        <source>Close &amp;all tabs</source>
        <translation type="unfinished">Đóng tất cả thẻ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2930"/>
        <source>Close all tabs &amp;but current tab</source>
        <translation type="unfinished">Đóng tất cả trù thẻ này</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2932"/>
        <source>Restore &amp;most recently closed tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2943"/>
        <source>&amp;Previous bookmark</source>
        <translation type="unfinished">Đánh dấu trướ&amp;c</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2944"/>
        <source>&amp;Next bookmark</source>
        <translation type="unfinished">Đánh dấ&amp;u tiếp</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2946"/>
        <source>&amp;Add bookmark</source>
        <translation type="unfinished">&amp;Thêm đánh dấu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3132"/>
        <source>Thumb&amp;nails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3146"/>
        <source>Book&amp;marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3218"/>
        <source>Composition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2195"/>
        <location filename="../sources/mainwindow.cpp" line="2947"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished">&amp;Bỏ đánh dấu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1910"/>
        <source>&lt;/ul&gt;&lt;p&gt;See &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; for more information.&lt;/p&gt;&lt;p&gt;&amp;copy; 2012-2015 The qpdfview developers&lt;/p&gt;</source>
        <translation type="unfinished">&lt;/ul&gt;&lt;p&gt;Xem tại &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; để biết thêm thông tin.&lt;/p&gt;&lt;p&gt;&amp;copy; 2012-2014 Nhóm phát triển qpdfview&lt;/p&gt; {2012-2015 ?}</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2670"/>
        <source>Edit &apos;%1&apos; at %2,%3...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2861"/>
        <source>Open containing &amp;folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2948"/>
        <source>Remove all bookmarks</source>
        <translation type="unfinished">Bỏ tất cả đánh dấu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2952"/>
        <source>&amp;Contents</source>
        <translation type="unfinished">&amp;Mục</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2953"/>
        <source>&amp;About</source>
        <translation type="unfinished">Thông t&amp;in</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2981"/>
        <location filename="../sources/mainwindow.cpp" line="3173"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Tệp</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2984"/>
        <location filename="../sources/mainwindow.cpp" line="3194"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">&amp;Sửa</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2987"/>
        <location filename="../sources/mainwindow.cpp" line="3207"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Xem</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3099"/>
        <source>&amp;Outline</source>
        <translation type="unfinished">&amp;Dàn bài</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3118"/>
        <source>&amp;Properties</source>
        <translation type="unfinished">T&amp;huộc tính</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3022"/>
        <source>&amp;Search</source>
        <translation type="unfinished">&amp;Tìm kiếm</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3224"/>
        <source>&amp;Tool bars</source>
        <translation type="unfinished">Thanh &amp;công cụ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3227"/>
        <source>&amp;Docks</source>
        <translation type="unfinished">Thanh &amp;neo</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3241"/>
        <source>&amp;Tabs</source>
        <translation type="unfinished">&amp;Thẻ</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3264"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished">Đánh &amp;dấu</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3275"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Giúp đỡ</translation>
    </message>
</context>
<context>
    <name>qpdfview::PageItem</name>
    <message>
        <location filename="../sources/pageitem.cpp" line="362"/>
        <source>Go to page %1.</source>
        <translation type="unfinished">Đến trang %1.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="366"/>
        <source>Go to page %1 of file &apos;%2&apos;.</source>
        <translation type="unfinished">Đến trang %1 hoặc &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="374"/>
        <source>Open &apos;%1&apos;.</source>
        <translation type="unfinished">Mở &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="409"/>
        <source>Edit form field &apos;%1&apos;.</source>
        <translation type="unfinished">Sửa biểu mẫu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="742"/>
        <source>Copy &amp;text</source>
        <translation type="unfinished">Sao chép &amp;văn bản</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="743"/>
        <source>&amp;Select text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="744"/>
        <source>Copy &amp;image</source>
        <translation type="unfinished">Sao chép &amp;hình</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="745"/>
        <source>Save image to &amp;file...</source>
        <translation type="unfinished">Lưu hình vào &amp;tệp...</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="780"/>
        <source>Save image to file</source>
        <translation type="unfinished">Lưu hình vào tệp</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="784"/>
        <source>Warning</source>
        <translation type="unfinished">Cảnh báo</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="784"/>
        <source>Could not save image to file &apos;%1&apos;.</source>
        <translation type="unfinished">Không thể lưu hình vào tệp &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="797"/>
        <source>Add &amp;text</source>
        <translation type="unfinished">Thêm văn bản</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="798"/>
        <source>Add &amp;highlight</source>
        <translation type="unfinished">Thêm tô sáng</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="840"/>
        <source>&amp;Copy link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="841"/>
        <source>&amp;Select link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="864"/>
        <source>&amp;Remove annotation</source>
        <translation type="unfinished">Bỏ ghi chú</translation>
    </message>
</context>
<context>
    <name>qpdfview::PdfSettingsWidget</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1007"/>
        <source>Antialiasing:</source>
        <translation type="unfinished">Khử răng cưa</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1014"/>
        <source>Text antialiasing:</source>
        <translation type="unfinished">Khử răng cưa cho văn bản</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1021"/>
        <location filename="../sources/pdfmodel.cpp" line="1060"/>
        <source>None</source>
        <translation type="unfinished">Trống</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1022"/>
        <source>Full</source>
        <translation type="unfinished">Đầy</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1023"/>
        <source>Reduced</source>
        <translation type="unfinished">Giảm bớt</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1026"/>
        <location filename="../sources/pdfmodel.cpp" line="1033"/>
        <source>Text hinting:</source>
        <translation type="unfinished">Gợi ý văn bản:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1042"/>
        <source>Ignore paper color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1053"/>
        <source>Overprint preview:</source>
        <translation type="unfinished">Xem trước khi in:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1061"/>
        <source>Solid</source>
        <translation type="unfinished">Đặc</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1062"/>
        <source>Shaped</source>
        <translation type="unfinished">Sắc nét</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1065"/>
        <source>Thin line mode:</source>
        <translation type="unfinished">Chế độ dòng mảnh:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1070"/>
        <source>Splash</source>
        <translation type="unfinished">Nháy</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1071"/>
        <source>Arthur</source>
        <translation type="unfinished">Arthur</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1074"/>
        <source>Backend:</source>
        <translation type="unfinished">Bổ trợ:</translation>
    </message>
</context>
<context>
    <name>qpdfview::PluginHandler</name>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="307"/>
        <source>Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="312"/>
        <source>Supported formats (%1)</source>
        <translation type="unfinished">Định dạng hỗ trợ (%1)</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="323"/>
        <source>Could not match file type of &apos;%1&apos;!</source>
        <translation type="unfinished">Không thể khớp loại tệp của &apos;%1&apos;!</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="330"/>
        <source>Critical</source>
        <translation type="unfinished">Xung đột</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="330"/>
        <source>Could not load plug-in for file type &apos;%1&apos;!</source>
        <translation type="unfinished">Không thể tải phần bổ trợ cho loại tệp &apos;%1&apos;!</translation>
    </message>
</context>
<context>
    <name>qpdfview::PrintDialog</name>
    <message>
        <location filename="../sources/printdialog.cpp" line="64"/>
        <source>Fit to page:</source>
        <translation type="unfinished">Vừa khít trang:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="68"/>
        <source>Page ranges:</source>
        <translation type="unfinished">Khoảng trang:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="73"/>
        <source>All pages</source>
        <translation type="unfinished">Tất cả trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="74"/>
        <source>Even pages</source>
        <translation type="unfinished">Trang sự kiện</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="75"/>
        <source>Odd pages</source>
        <translation type="unfinished">Trang odd</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="78"/>
        <source>Page set:</source>
        <translation type="unfinished">Trang đặt:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="81"/>
        <source>Single page</source>
        <translation type="unfinished">1 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="82"/>
        <source>Two pages</source>
        <translation type="unfinished">2 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="83"/>
        <source>Four pages</source>
        <translation type="unfinished">4 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="84"/>
        <source>Six pages</source>
        <translation type="unfinished">6 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="85"/>
        <source>Nine pages</source>
        <translation type="unfinished">9 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="86"/>
        <source>Sixteen pages</source>
        <translation type="unfinished">16 trang</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="89"/>
        <source>Number-up:</source>
        <translation type="unfinished">Số-trên:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="92"/>
        <source>Bottom to top and left to right</source>
        <translation type="unfinished">Dưới lên trên và trái qua phải</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="93"/>
        <source>Bottom to top and right to left</source>
        <translation type="unfinished">Dưới lên trên và phải qua trái</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="94"/>
        <source>Left to right and bottom to top</source>
        <translation type="unfinished">Trái qua phải và dưới lên trên</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="95"/>
        <source>Left to right and top to bottom</source>
        <translation type="unfinished">Trái qua phải và trên xuống dưới</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="96"/>
        <source>Right to left and bottom to top</source>
        <translation type="unfinished">Phải qua trái và dưới lên trên</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="97"/>
        <source>Right to left and top to bottom</source>
        <translation type="unfinished">Phải qua trái và trên xuống dưới</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="98"/>
        <source>Top to bottom and left to right</source>
        <translation type="unfinished">Trên xuống dưới và trái qua phải</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="99"/>
        <source>Top to bottom and right to left</source>
        <translation type="unfinished">Trên xuống dưới và phải qua trái</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="102"/>
        <source>Number-up layout:</source>
        <translation type="unfinished">Number-up layout:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="106"/>
        <source>Extended options</source>
        <translation type="unfinished">Tùy chọn mở rộng</translation>
    </message>
</context>
<context>
    <name>qpdfview::PsSettingsWidget</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="262"/>
        <source>Graphics antialias bits:</source>
        <translation type="unfinished">Số bit khử răng cưa đồ họa:</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="270"/>
        <source>Text antialias bits:</source>
        <translation type="unfinished">Số bit khử răng cưa văn bản:</translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyClosedMenu</name>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="32"/>
        <source>&amp;Recently closed</source>
        <translation type="unfinished">Hiện tại đã đóng</translation>
    </message>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="39"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished">&amp;Xóa danh sách</translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyUsedMenu</name>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="32"/>
        <source>Recently &amp;used</source>
        <translation type="unfinished">Hiện tại đang dùng</translation>
    </message>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished">&amp;Xóa danh sách</translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchModel</name>
    <message>
        <location filename="../sources/searchmodel.cpp" line="148"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/searchmodel.cpp" line="182"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences on page &lt;b&gt;%2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchableMenu</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="181"/>
        <source>Search for &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SettingsDialog</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="108"/>
        <source>General</source>
        <translation type="unfinished">Chung</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="141"/>
        <source>&amp;Behavior</source>
        <translation type="unfinished">&amp;Hành vi</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="142"/>
        <source>&amp;Graphics</source>
        <translation type="unfinished">Đồ &amp;họa</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="143"/>
        <source>&amp;Interface</source>
        <translation type="unfinished">&amp;Giao diện</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="144"/>
        <source>&amp;Shortcuts</source>
        <translation type="unfinished">&amp;Phím tắt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="145"/>
        <source>&amp;Modifiers</source>
        <translation type="unfinished">&amp;Bổ trợ</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="151"/>
        <source>Defaults</source>
        <translation type="unfinished">Mặc định</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="154"/>
        <source>Defaults on current tab</source>
        <translation type="unfinished">Mặc định từ thẻ này</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="160"/>
        <source>Mouse wheel modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="163"/>
        <source>Mouse button modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="244"/>
        <source>Open URL:</source>
        <translation type="unfinished">Mở URL:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="248"/>
        <source>Auto-refresh:</source>
        <translation type="unfinished">Tự làm tươi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="252"/>
        <location filename="../sources/settingsdialog.cpp" line="255"/>
        <location filename="../sources/settingsdialog.cpp" line="518"/>
        <location filename="../sources/settingsdialog.cpp" line="549"/>
        <location filename="../sources/settingsdialog.cpp" line="552"/>
        <location filename="../sources/settingsdialog.cpp" line="556"/>
        <location filename="../sources/settingsdialog.cpp" line="559"/>
        <location filename="../sources/settingsdialog.cpp" line="562"/>
        <location filename="../sources/settingsdialog.cpp" line="571"/>
        <source>Effective after restart.</source>
        <translation type="unfinished">Có hiệu lực sau khi khởi động lại</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="252"/>
        <source>Track recently used:</source>
        <translation type="unfinished">Khe hiện tại được sử dụng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="255"/>
        <source>Keep recently closed:</source>
        <translation type="unfinished">Giữ hiện tại đã đóng</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="259"/>
        <source>Restore tabs:</source>
        <translation type="unfinished">Khôi phục thẻ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="262"/>
        <source>Restore bookmarks:</source>
        <translation type="unfinished">Khôi phục đánh dấu:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="265"/>
        <source>Restore per-file settings:</source>
        <translation type="unfinished">Khôi phục thiết đặt mỗi tệp:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source> min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source>Save database interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="281"/>
        <source>Synchronize presentation:</source>
        <translation type="unfinished">Đồng bộ hóa trình chiếu:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="284"/>
        <source>Default</source>
        <translation type="unfinished">Mặc định</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="284"/>
        <source>Presentation screen:</source>
        <translation type="unfinished">Màn hình trình chiếu:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="295"/>
        <source>Zoom factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source> ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source>None</source>
        <translation type="unfinished">Trống</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source>Highlight duration:</source>
        <translation type="unfinished">Thời gian tô sáng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="302"/>
        <source>Highlight color:</source>
        <translation type="unfinished">Màu tô sáng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="305"/>
        <source>Annotation color:</source>
        <translation type="unfinished">Màu ghi chú:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="309"/>
        <source>&apos;%1&apos; is replaced by the absolute file path. &apos;%2&apos; resp. &apos;%3&apos; is replaced by line resp. column number.</source>
        <translation type="unfinished">&apos;%1&apos; được thay bằng đường dẫn tập tin tuyệt đối. &apos;%2&apos; resp. &apos;%3&apos; được thay thế bằng dòng resp. số cột.</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="309"/>
        <source>Source editor:</source>
        <translation type="unfinished">Soạn thảo mã nguồn:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="373"/>
        <source>Use tiling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="376"/>
        <source>Keep obsolete pixmaps:</source>
        <translation type="unfinished">Giữ pixmap lỗi thời:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="379"/>
        <source>Use device pixel ratio:</source>
        <translation type="unfinished">Sử dụng tỉ lệ pixel của thiết bị</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="389"/>
        <source>Decorate pages:</source>
        <translation type="unfinished">Decorate pages:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="392"/>
        <source>Decorate links:</source>
        <translation type="unfinished">Decorate links:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="395"/>
        <source>Decorate form fields:</source>
        <translation type="unfinished">Decorate form fields:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="399"/>
        <source>Background color:</source>
        <translation type="unfinished">Màu nền:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="402"/>
        <source>Paper color:</source>
        <translation type="unfinished">Màu giấy:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="405"/>
        <source>Presentation background color:</source>
        <translation type="unfinished">Màu nền trình chiếu:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="409"/>
        <source>Pages per row:</source>
        <translation type="unfinished">Số trang mỗi dòng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="413"/>
        <location filename="../sources/settingsdialog.cpp" line="416"/>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="413"/>
        <source>Page spacing:</source>
        <translation type="unfinished">Khoảng cách trang:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="416"/>
        <source>Thumbnail spacing:</source>
        <translation type="unfinished">Khoảng cách hình thu nhỏ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source>Thumbnail size:</source>
        <translation type="unfinished">Kích cỡ hình thu nhỏ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source>Fit to viewport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Document context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="568"/>
        <source>Tab context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="701"/>
        <source>Open in source editor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="810"/>
        <location filename="../sources/settingsdialog.cpp" line="811"/>
        <location filename="../sources/settingsdialog.cpp" line="812"/>
        <location filename="../sources/settingsdialog.cpp" line="813"/>
        <location filename="../sources/settingsdialog.cpp" line="814"/>
        <location filename="../sources/settingsdialog.cpp" line="815"/>
        <location filename="../sources/settingsdialog.cpp" line="816"/>
        <location filename="../sources/settingsdialog.cpp" line="817"/>
        <location filename="../sources/settingsdialog.cpp" line="818"/>
        <location filename="../sources/settingsdialog.cpp" line="819"/>
        <location filename="../sources/settingsdialog.cpp" line="827"/>
        <source>%1 MB</source>
        <translation type="unfinished">%1 Mb</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="424"/>
        <source>Cache size:</source>
        <translation type="unfinished">Kích thước đệm</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="292"/>
        <source>Minimal scrolling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="427"/>
        <source>Prefetch:</source>
        <translation type="unfinished">Prefetch:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="430"/>
        <source>Prefetch distance:</source>
        <translation type="unfinished">Prefetch distance:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Top</source>
        <translation type="unfinished">Trên</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Bottom</source>
        <translation type="unfinished">Dưới</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Left</source>
        <translation type="unfinished">Trái</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Right</source>
        <translation type="unfinished">Phải</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="528"/>
        <source>Tab position:</source>
        <translation type="unfinished">Vị trí thẻ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>As needed</source>
        <translation type="unfinished">Nếu cần thiết</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>Always</source>
        <translation type="unfinished">Luôn luôn</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="545"/>
        <source>Exit after last tab:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="571"/>
        <source>Scrollable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="574"/>
        <source>Searchable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="698"/>
        <source>Zoom to selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>Never</source>
        <translation type="unfinished">Không bao giờ</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="105"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="533"/>
        <source>Tab visibility:</source>
        <translation type="unfinished">Ẩn thẻ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="538"/>
        <source>Spread tabs:</source>
        <translation type="unfinished">Dàn trải thẻ</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="542"/>
        <source>New tab next to current tab:</source>
        <translation type="unfinished">Thẻ mới đến thẻ này:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="549"/>
        <source>Recently used count:</source>
        <translation type="unfinished">Đếm số hiện tại đang sử dụng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="552"/>
        <source>Recently closed count:</source>
        <translation type="unfinished">Số lượng hiện tại đã đóng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="518"/>
        <source>Extended search dock:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="578"/>
        <source>Toggle tool and menu bars with fullscreen:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="556"/>
        <source>File tool bar:</source>
        <translation type="unfinished">Thanh công cụ tệp:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="559"/>
        <source>Edit tool bar:</source>
        <translation type="unfinished">Thanh công cụ chỉnh sửa:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="562"/>
        <source>View tool bar:</source>
        <translation type="unfinished">Thanh công cụ xem:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="582"/>
        <source>Use page label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <source>Document title as tab title:</source>
        <translation type="unfinished">Tiêu đề tài liệu như tiêu đề thẻ</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="589"/>
        <source>Current page in window title:</source>
        <translation type="unfinished">Trang hiện tại trong tiêu đề cửa sổ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <source>Instance name in window title:</source>
        <translation type="unfinished">Tên yêu cầu trong tiêu đề cửa sổ:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="288"/>
        <source>Synchronize outline view:</source>
        <translation type="unfinished">Đồng bộ hóa xem phác thảo:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="596"/>
        <source>Highlight current thumbnail:</source>
        <translation type="unfinished">Làm nổi bật hình ảnh thu nhỏ hiện tại:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="599"/>
        <source>Limit thumbnails to results:</source>
        <translation type="unfinished">Giới hạn hình thu nhỏ đến:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="521"/>
        <source>Annotation overlay:</source>
        <translation type="unfinished">Annotation overlay:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="524"/>
        <source>Form field overlay:</source>
        <translation type="unfinished">Form field overlay:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="683"/>
        <source>Zoom:</source>
        <translation type="unfinished">Thu phóng:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="686"/>
        <source>Rotate:</source>
        <translation type="unfinished">Xoay:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="689"/>
        <source>Scroll:</source>
        <translation type="unfinished">Cuộn:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="692"/>
        <source>Copy to clipboard:</source>
        <translation type="unfinished">Chép vào bộ nhớ đệm:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="695"/>
        <source>Add annotation:</source>
        <translation type="unfinished">Thêm chú thích:</translation>
    </message>
</context>
<context>
    <name>qpdfview::ShortcutHandler</name>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="148"/>
        <source>Action</source>
        <translation type="unfinished">Hành động</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="151"/>
        <source>Key sequence</source>
        <translation type="unfinished">Dãy phím</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="269"/>
        <source>Skip backward</source>
        <translation type="unfinished">Bỏ trước đó</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="276"/>
        <source>Skip forward</source>
        <translation type="unfinished">Bỏ kế tiếp</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="283"/>
        <source>Move up</source>
        <translation type="unfinished">Di chuyển lên</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="290"/>
        <source>Move down</source>
        <translation type="unfinished">Di chuyển xuống</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="297"/>
        <source>Move left</source>
        <translation type="unfinished">Di chuyển qua trái</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="304"/>
        <source>Move right</source>
        <translation type="unfinished">Di chuyển qua phải</translation>
    </message>
</context>
<context>
    <name>qpdfview::TreeView</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="632"/>
        <source>&amp;Expand all</source>
        <translation type="unfinished">&amp;Bung ra</translation>
    </message>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="633"/>
        <source>&amp;Collapse all</source>
        <translation type="unfinished">&amp;Thu lại</translation>
    </message>
</context>
</TS>
