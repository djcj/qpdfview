<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Model::ImageDocument</name>
    <message>
        <location filename="../sources/imagemodel.cpp" line="126"/>
        <source>Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="154"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="155"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="156"/>
        <source>Depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="164"/>
        <location filename="../sources/imagemodel.cpp" line="167"/>
        <location filename="../sources/imagemodel.cpp" line="170"/>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <location filename="../sources/imagemodel.cpp" line="178"/>
        <location filename="../sources/imagemodel.cpp" line="182"/>
        <source>Format</source>
        <translation type="unfinished">Format</translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="164"/>
        <source>Monochrome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="167"/>
        <source>Indexed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="170"/>
        <source>32 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="173"/>
        <source>32 bits ARGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="178"/>
        <source>16 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/imagemodel.cpp" line="182"/>
        <source>24 bits RGB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Model::PdfDocument</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <source>Linearized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Name</source>
        <translation type="unfinished">Ad</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Type</source>
        <translation type="unfinished">Növ</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Embedded</source>
        <translation type="unfinished">İçərisinə yerləşdirilib</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>Subset</source>
        <translation type="unfinished">Alt qrup</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="927"/>
        <source>File</source>
        <translation type="unfinished">Fayl</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <location filename="../sources/pdfmodel.cpp" line="935"/>
        <location filename="../sources/pdfmodel.cpp" line="936"/>
        <source>Yes</source>
        <translation type="unfinished">Bəli</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="910"/>
        <source>PDF version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <source>Encrypted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="912"/>
        <location filename="../sources/pdfmodel.cpp" line="913"/>
        <location filename="../sources/pdfmodel.cpp" line="935"/>
        <location filename="../sources/pdfmodel.cpp" line="936"/>
        <source>No</source>
        <translation type="unfinished">Xeyr</translation>
    </message>
</context>
<context>
    <name>Model::PdfPage</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="599"/>
        <source>Information</source>
        <translation type="unfinished">Mə&apos;lumat</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="599"/>
        <source>Version 0.20.1 or higher of the Poppler library is required to add or remove annotations.</source>
        <translation type="unfinished">Əlfəcinlərin alavə edilməsi və ya silinməsi üçün Popler 0.20.1 və ya daha yüksək versiya kitabxana tələb edir.</translation>
    </message>
</context>
<context>
    <name>Model::PsDocument</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="241"/>
        <source>Title</source>
        <translation type="unfinished">Başlıq</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="242"/>
        <source>Created for</source>
        <translation type="unfinished">Kimin üçün yaradılıb</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="243"/>
        <source>Creator</source>
        <translation type="unfinished">Yaradan</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="244"/>
        <source>Creation date</source>
        <translation type="unfinished">Yaradılma tarixi</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="245"/>
        <source>Format</source>
        <translation type="unfinished">Format</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="246"/>
        <source>Language level</source>
        <translation type="unfinished">Dil səviyyəsi</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/main.cpp" line="152"/>
        <source>An empty instance name is not allowed.</source>
        <translation type="unfinished">Boş instansiya adına icazə yoxdur.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="163"/>
        <source>An empty search text is not allowed.</source>
        <translation type="unfinished">Axtarış mətni boşdur.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="191"/>
        <source>Choose instance</source>
        <translation type="unfinished">Nüsxəni bağla</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="191"/>
        <source>Instance:</source>
        <translation type="unfinished">Nüsxə</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="220"/>
        <source>Unknown command-line option &apos;%1&apos;.</source>
        <translation type="unfinished">Naməlum əmr-sətri opsiyası &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="251"/>
        <source>Using &apos;--instance&apos; requires an instance name.</source>
        <translation type="unfinished">&apos;--instance&apos; istifadə etdikdə instansiya adı mütləq göstərilməlidir.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="257"/>
        <source>Using &apos;--instance&apos; is not allowed without using &apos;--unique&apos;.</source>
        <translation type="unfinished">&apos;--instance&apos; -dən istifadə &apos;--unique&apos; istifadə etmədən mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="263"/>
        <source>An instance name must only contain the characters &quot;[A-Z][a-z][0-9]_&quot; and must not begin with a digit.</source>
        <translation type="unfinished">Nüsxənin adında yalnız &quot;[A-Z][a-z][0-9]_&quot; işarələri olmalı və rəqəmlə başlamamalıdır.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="269"/>
        <source>Using &apos;--search&apos; requires a search text.</source>
        <translation type="unfinished">&apos;--search&apos; istifadə etdikdə axtarılan mətn daxil edilməlidir.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="347"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">&apos;%1&apos;  üçün SyncTeX verilənləri yüklənmədi.</translation>
    </message>
    <message>
        <location filename="../sources/main.cpp" line="446"/>
        <source>Could not prepare signal handler.</source>
        <translation type="unfinished">Siqnal başlığını hazırlamaq mümkün deyil.</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="856"/>
        <source>Shift</source>
        <translation type="unfinished">Şift</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="857"/>
        <source>Ctrl</source>
        <translation type="unfinished">Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="858"/>
        <source>Alt</source>
        <translation type="unfinished">Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="859"/>
        <source>Shift and Ctrl</source>
        <translation type="unfinished">Şift və Ctrl</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="860"/>
        <source>Shift and Alt</source>
        <translation type="unfinished">Şift və Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="861"/>
        <source>Ctrl and Alt</source>
        <translation type="unfinished">Ctrl və Alt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="862"/>
        <source>Right mouse button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="863"/>
        <source>Middle mouse button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="864"/>
        <source>None</source>
        <translation type="unfinished">Heç Biri</translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkDialog</name>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="39"/>
        <source>Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="48"/>
        <source>Page:</source>
        <translation type="unfinished">Səhifə:</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="53"/>
        <source>Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="58"/>
        <source>Comment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/bookmarkdialog.cpp" line="64"/>
        <source>Modified:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::BookmarkMenu</name>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="39"/>
        <source>&amp;Open</source>
        <translation type="unfinished">&amp;Aç</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="44"/>
        <source>Open in new &amp;tab</source>
        <translation type="unfinished">Yeni &amp;İçlikdə aç</translation>
    </message>
    <message>
        <location filename="../sources/bookmarkmenu.cpp" line="54"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::Database</name>
    <message>
        <location filename="../sources/database.cpp" line="910"/>
        <source>Jump to page %1</source>
        <translation type="unfinished">%1 səhifəsinə keç</translation>
    </message>
</context>
<context>
    <name>qpdfview::DocumentView</name>
    <message>
        <location filename="../sources/documentview.cpp" line="1006"/>
        <location filename="../sources/documentview.cpp" line="1633"/>
        <source>Information</source>
        <translation type="unfinished">Mə&apos;lumat</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1006"/>
        <source>The source editor has not been set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1633"/>
        <source>Opening URL is disabled in the settings.</source>
        <translation type="unfinished">URL-lərin açılması seçənklərdən bağlıdır</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1673"/>
        <source>Warning</source>
        <translation type="unfinished">Xəbərdarlıq</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="1673"/>
        <source>SyncTeX data for &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">&apos;%1&apos;  üçün SyncTeX verilənləri yüklənmədi.</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2141"/>
        <source>Printing &apos;%1&apos;...</source>
        <translation type="unfinished">&apos;%1&apos; çap olunur...</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2215"/>
        <source>Unlock %1</source>
        <translation type="unfinished">%1 aç</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2215"/>
        <source>Password:</source>
        <translation type="unfinished">Şifrə:</translation>
    </message>
    <message>
        <location filename="../sources/documentview.cpp" line="2257"/>
        <source>Page %1</source>
        <translation type="unfinished">Səhifə %1</translation>
    </message>
</context>
<context>
    <name>qpdfview::FileAttachmentAnnotationWidget</name>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="112"/>
        <source>Save...</source>
        <translation type="unfinished">Yaddaşa al</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="113"/>
        <source>Save and open...</source>
        <translation type="unfinished">Yaddaşa al və aç…</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="160"/>
        <source>Save file attachment</source>
        <translation type="unfinished">Əlavədəki faylı saxla</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="176"/>
        <location filename="../sources/annotationwidgets.cpp" line="182"/>
        <source>Warning</source>
        <translation type="unfinished">Xəbərdarlıq</translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="176"/>
        <source>Could not open file attachment saved to &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/annotationwidgets.cpp" line="182"/>
        <source>Could not save file attachment to &apos;%1&apos;.</source>
        <translation type="unfinished">Əlavədəki faylı  &apos;%1&apos; yaddaça almaq mümkün deyil</translation>
    </message>
</context>
<context>
    <name>qpdfview::FontsDialog</name>
    <message>
        <location filename="../sources/fontsdialog.cpp" line="37"/>
        <source>Fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::HelpDialog</name>
    <message>
        <location filename="../sources/helpdialog.cpp" line="40"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="47"/>
        <source>help.html</source>
        <extracomment>Please replace by file name of localized help if available, e.g. &quot;help_fr.html&quot;.</extracomment>
        <translation type="unfinished">help.html</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="62"/>
        <source>Find previous</source>
        <translation type="unfinished">Öncəkini tap</translation>
    </message>
    <message>
        <location filename="../sources/helpdialog.cpp" line="66"/>
        <source>Find next</source>
        <translation type="unfinished">Sonrakını tap</translation>
    </message>
</context>
<context>
    <name>qpdfview::MainWindow</name>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2957"/>
        <source>Toggle tool bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2958"/>
        <source>Toggle menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="363"/>
        <location filename="../sources/mainwindow.cpp" line="446"/>
        <location filename="../sources/mainwindow.cpp" line="1219"/>
        <location filename="../sources/mainwindow.cpp" line="1236"/>
        <location filename="../sources/mainwindow.cpp" line="1253"/>
        <location filename="../sources/mainwindow.cpp" line="1291"/>
        <location filename="../sources/mainwindow.cpp" line="1434"/>
        <location filename="../sources/mainwindow.cpp" line="2561"/>
        <location filename="../sources/mainwindow.cpp" line="2575"/>
        <source>Warning</source>
        <translation type="unfinished">Xəbərdarlıq</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="363"/>
        <location filename="../sources/mainwindow.cpp" line="446"/>
        <source>Could not open &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; açmaq mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="683"/>
        <source>Copy file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="684"/>
        <source>Select file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="686"/>
        <source>Close all tabs</source>
        <translation type="unfinished">Bütün səkmələri bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="687"/>
        <source>Close all tabs but this one</source>
        <translation type="unfinished">Bu siğmədən başqa hamısını bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="688"/>
        <source>Close all tabs to the left</source>
        <translation type="unfinished">Solda olan bütün siğmələri bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="689"/>
        <source>Close all tabs to the right</source>
        <translation type="unfinished">Sağda olan bütün siğmələri bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1164"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1180"/>
        <source>Open in new tab</source>
        <translation type="unfinished">Yeni İçlikdə aç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1219"/>
        <location filename="../sources/mainwindow.cpp" line="1434"/>
        <source>Could not refresh &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; yüniləmək mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1226"/>
        <source>Save copy</source>
        <translation type="unfinished">Nüsxəsini yadda saxla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1236"/>
        <source>Could not save copy at &apos;%1&apos;.</source>
        <translation type="unfinished">Kopyanı  &apos;%1&apos; saxlamaq mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1243"/>
        <location filename="../sources/mainwindow.cpp" line="2565"/>
        <source>Save as</source>
        <translation type="unfinished">Fərqli adla yaddaşda saxla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1253"/>
        <location filename="../sources/mainwindow.cpp" line="2575"/>
        <source>Could not save as &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; kimi yadda saxlamaq mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1291"/>
        <source>Could not print &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; çap etmək mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1327"/>
        <source>Set first page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1327"/>
        <source>Select the first page of the body matter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1340"/>
        <source>Jump to page</source>
        <translation type="unfinished">Səhifəyə keç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1340"/>
        <source>Page:</source>
        <translation type="unfinished">Səhifə:</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1741"/>
        <source>Jump to page %1</source>
        <translation type="unfinished">%1 səhifəsinə keç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1892"/>
        <source>About qpdfview</source>
        <translation type="unfinished">qpdfview haqda</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1892"/>
        <source>&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview is a tabbed document viewer using Qt.&lt;/p&gt;&lt;p&gt;This version includes:&lt;ul&gt;</source>
        <translation type="unfinished">&lt;p&gt;&lt;b&gt;qpdfview %1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;qpdfview Qt-dən istifadə edən səkməli sənəd baxıcısıdır.&lt;/p&gt;&lt;p&gt;Bu versiyaya daxildir:&lt;ul&gt;</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1896"/>
        <source>&lt;li&gt;PDF support using Poppler %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1899"/>
        <source>&lt;li&gt;PS support using libspectre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1902"/>
        <source>&lt;li&gt;DjVu support using DjVuLibre %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1905"/>
        <source>&lt;li&gt;PDF support using Fitz %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1908"/>
        <source>&lt;li&gt;Printing support using CUPS %1&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2196"/>
        <source>&amp;Edit bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2561"/>
        <source>The document &apos;%1&apos; has been modified. Do you want to save your changes?</source>
        <translation type="unfinished">&apos;%1&apos; sənədi dəyişdirilmişdir. Dəyişiklikləri yadda saxlamaq istəyirsiniz mi?</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2770"/>
        <source>Page width</source>
        <translation type="unfinished">Səhifənin eni</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2771"/>
        <source>Page size</source>
        <translation type="unfinished">Səhifənin ölçüsü</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2794"/>
        <source>Match &amp;case</source>
        <translation type="unfinished">Reqistr nəzərə alınmaqla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2795"/>
        <source>Whole &amp;words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2796"/>
        <source>Highlight &amp;all</source>
        <translation type="unfinished">H&amp;amısını seç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2858"/>
        <source>&amp;Open...</source>
        <translation type="unfinished">&amp;Aç...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2859"/>
        <source>Open in new &amp;tab...</source>
        <translation type="unfinished">Yeni İçlikdə aç...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2860"/>
        <source>Open &amp;copy in new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2862"/>
        <source>&amp;Refresh</source>
        <translation type="unfinished">&amp;Yenilə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2863"/>
        <source>&amp;Save copy...</source>
        <translation type="unfinished">Nüsxəsini yaddaşda saxla...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2864"/>
        <source>Save &amp;as...</source>
        <translation type="unfinished">Başqa cür yadda saxla...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2865"/>
        <source>&amp;Print...</source>
        <translation type="unfinished">Ça&amp;p Et...</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2866"/>
        <source>E&amp;xit</source>
        <translation type="unfinished">Çı&amp;xış</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2870"/>
        <source>&amp;Previous page</source>
        <translation type="unfinished">Əlvəlki səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2871"/>
        <source>&amp;Next page</source>
        <translation type="unfinished">Növbəti səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2872"/>
        <source>&amp;First page</source>
        <translation type="unfinished">Öncəki səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2873"/>
        <source>&amp;Last page</source>
        <translation type="unfinished">Sonuncu səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2875"/>
        <source>&amp;Set first page...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2877"/>
        <source>&amp;Jump to page...</source>
        <translation type="unfinished">Səhifəyə keç…</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2879"/>
        <source>Jump &amp;backward</source>
        <translation type="unfinished">Geri qayıt</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2880"/>
        <source>Jump for&amp;ward</source>
        <translation type="unfinished">İrəli keç</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2882"/>
        <source>&amp;Search...</source>
        <translation type="unfinished">Axtarış</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2883"/>
        <source>Find previous</source>
        <translation type="unfinished">Öncəkini tap</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2884"/>
        <source>Find next</source>
        <translation type="unfinished">Sonrakını tap</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2885"/>
        <source>Cancel search</source>
        <translation type="unfinished">Axtarışdan imtina</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2887"/>
        <source>&amp;Copy to clipboard</source>
        <translation type="unfinished">Yaddaşa kopyala</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2888"/>
        <source>&amp;Add annotation</source>
        <translation type="unfinished">Qeydlər əlavə et</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2890"/>
        <source>Settings...</source>
        <translation type="unfinished">Seçənəklər…</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2894"/>
        <source>&amp;Continuous</source>
        <translation type="unfinished">Davamlı</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2895"/>
        <source>&amp;Two pages</source>
        <translation type="unfinished">İki səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2896"/>
        <source>Two pages &amp;with cover page</source>
        <translation type="unfinished">Başlıqla &amp;birgə iki səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2897"/>
        <source>&amp;Multiple pages</source>
        <translation type="unfinished">Bir neçə səhifə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2899"/>
        <source>Right to left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2901"/>
        <source>Zoom &amp;in</source>
        <translation type="unfinished">Böyüt</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2902"/>
        <source>Zoom &amp;out</source>
        <translation type="unfinished">Kiçild</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2903"/>
        <source>Original &amp;size</source>
        <translation type="unfinished">Oriqinal ölçü</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2905"/>
        <source>Fit to page width</source>
        <translation type="unfinished">Səhifə eninə uyğunlaşdır</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2906"/>
        <source>Fit to page size</source>
        <translation type="unfinished">Səhifə ölçüsünə uyğunlaşdır</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2908"/>
        <source>Rotate &amp;left</source>
        <translation type="unfinished">Sola çevir</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2909"/>
        <source>Rotate &amp;right</source>
        <translation type="unfinished">Sağa çevir</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2911"/>
        <source>Invert colors</source>
        <translation type="unfinished">Rəngləri çevir</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2912"/>
        <source>Convert to grayscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2913"/>
        <source>Trim margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2915"/>
        <source>Darken with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2916"/>
        <source>Lighten with paper color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2918"/>
        <source>Fonts...</source>
        <translation type="unfinished">Şriftlər…</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2920"/>
        <source>&amp;Fullscreen</source>
        <translation type="unfinished">&amp;Tam ekran</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2921"/>
        <source>&amp;Presentation...</source>
        <translation type="unfinished">Nümayiş…</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2925"/>
        <source>&amp;Previous tab</source>
        <translation type="unfinished">Öncəki səkmə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2926"/>
        <source>&amp;Next tab</source>
        <translation type="unfinished">Sonrakı səkmə</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2928"/>
        <source>&amp;Close tab</source>
        <translation type="unfinished">Səməni bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2929"/>
        <source>Close &amp;all tabs</source>
        <translation type="unfinished">Bütün səkmələri bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2930"/>
        <source>Close all tabs &amp;but current tab</source>
        <translation type="unfinished">Hazırkı səkmədən başqa, qalan səkmələri bağla</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2932"/>
        <source>Restore &amp;most recently closed tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2943"/>
        <source>&amp;Previous bookmark</source>
        <translation type="unfinished">Öncəki əıfəcin</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2944"/>
        <source>&amp;Next bookmark</source>
        <translation type="unfinished">Sonrakı əlfəcin</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2946"/>
        <source>&amp;Add bookmark</source>
        <translation type="unfinished">Əlfəcin əlavə et</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3132"/>
        <source>Thumb&amp;nails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3146"/>
        <source>Book&amp;marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3218"/>
        <source>Composition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2195"/>
        <location filename="../sources/mainwindow.cpp" line="2947"/>
        <source>&amp;Remove bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="1910"/>
        <source>&lt;/ul&gt;&lt;p&gt;See &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; for more information.&lt;/p&gt;&lt;p&gt;&amp;copy; 2012-2015 The qpdfview developers&lt;/p&gt;</source>
        <translation type="unfinished">&lt;/ul&gt;&lt;p&gt;Daha geniş məlumat üçün &lt;a href=&quot;https://launchpad.net/qpdfview&quot;&gt;launchpad.net/qpdfview&lt;/a&gt; bax.&lt;/p&gt;&lt;p&gt;&amp;copy; 2012-2014 The qpdfview proqramçıları.&lt;/p&gt; {2012-2015 ?}</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2670"/>
        <source>Edit &apos;%1&apos; at %2,%3...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2861"/>
        <source>Open containing &amp;folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2948"/>
        <source>Remove all bookmarks</source>
        <translation type="unfinished">Bütün əlfəcinləri sil</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2952"/>
        <source>&amp;Contents</source>
        <translation type="unfinished">&amp;Məzmun</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2953"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;Haqqında</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2981"/>
        <location filename="../sources/mainwindow.cpp" line="3173"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fayl</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2984"/>
        <location filename="../sources/mainwindow.cpp" line="3194"/>
        <source>&amp;Edit</source>
        <translation type="unfinished">Redaktə et</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="2987"/>
        <location filename="../sources/mainwindow.cpp" line="3207"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Görünüş</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3099"/>
        <source>&amp;Outline</source>
        <translation type="unfinished">Altdan xətli</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3118"/>
        <source>&amp;Properties</source>
        <translation type="unfinished">Xüsusiyyətlər</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3022"/>
        <source>&amp;Search</source>
        <translation type="unfinished">A&amp;xtar</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3224"/>
        <source>&amp;Tool bars</source>
        <translation type="unfinished">Alətlər paneli</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3227"/>
        <source>&amp;Docks</source>
        <translation type="unfinished">Doklar</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3241"/>
        <source>&amp;Tabs</source>
        <translation type="unfinished">Səkmələr</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3264"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished">Əlfəcinlər</translation>
    </message>
    <message>
        <location filename="../sources/mainwindow.cpp" line="3275"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Yardım</translation>
    </message>
</context>
<context>
    <name>qpdfview::PageItem</name>
    <message>
        <location filename="../sources/pageitem.cpp" line="362"/>
        <source>Go to page %1.</source>
        <translation type="unfinished">%1 səhifəsinə keç.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="366"/>
        <source>Go to page %1 of file &apos;%2&apos;.</source>
        <translation type="unfinished">&apos;%2&apos; faylının %1 səhifəsinə keç</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="374"/>
        <source>Open &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; aç.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="409"/>
        <source>Edit form field &apos;%1&apos;.</source>
        <translation type="unfinished">&apos;%1&apos; xanasından redaktə et.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="742"/>
        <source>Copy &amp;text</source>
        <translation type="unfinished">Mə&amp;tni kopyala</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="743"/>
        <source>&amp;Select text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="744"/>
        <source>Copy &amp;image</source>
        <translation type="unfinished">Şəkl&amp;i kopyala</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="745"/>
        <source>Save image to &amp;file...</source>
        <translation type="unfinished">Şəkli &amp;fayla yaz…</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="780"/>
        <source>Save image to file</source>
        <translation type="unfinished">Şəkli fayla yaz</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="784"/>
        <source>Warning</source>
        <translation type="unfinished">Xəbərdarlıq</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="784"/>
        <source>Could not save image to file &apos;%1&apos;.</source>
        <translation type="unfinished">Şəkli  &apos;%1&apos; faylına yazmaq mümkün deyil.</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="797"/>
        <source>Add &amp;text</source>
        <translation type="unfinished">Mətn əlavə e&amp;t</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="798"/>
        <source>Add &amp;highlight</source>
        <translation type="unfinished">Seçim əlavə et</translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="840"/>
        <source>&amp;Copy link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="841"/>
        <source>&amp;Select link address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pageitem.cpp" line="864"/>
        <source>&amp;Remove annotation</source>
        <translation type="unfinished">Qeydlə&amp;ri sil</translation>
    </message>
</context>
<context>
    <name>qpdfview::PdfSettingsWidget</name>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1007"/>
        <source>Antialiasing:</source>
        <translation type="unfinished">Hamarlama:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1014"/>
        <source>Text antialiasing:</source>
        <translation type="unfinished">Mətnin görünüşünün hamarlanması:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1021"/>
        <location filename="../sources/pdfmodel.cpp" line="1060"/>
        <source>None</source>
        <translation type="unfinished">Heç Biri</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1022"/>
        <source>Full</source>
        <translation type="unfinished">Tam</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1023"/>
        <source>Reduced</source>
        <translation type="unfinished">Azaldılmış</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1026"/>
        <location filename="../sources/pdfmodel.cpp" line="1033"/>
        <source>Text hinting:</source>
        <translation type="unfinished">Mətn bildirişi</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1042"/>
        <source>Ignore paper color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1053"/>
        <source>Overprint preview:</source>
        <translation type="unfinished">Çap həddini aşma ön baxışı</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1061"/>
        <source>Solid</source>
        <translation type="unfinished">Tam</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1062"/>
        <source>Shaped</source>
        <translation type="unfinished">Fromalı</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1065"/>
        <source>Thin line mode:</source>
        <translation type="unfinished">Xəttin nazikliyi:</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1070"/>
        <source>Splash</source>
        <translation type="unfinished">Tanıtım</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1071"/>
        <source>Arthur</source>
        <translation type="unfinished">Artur</translation>
    </message>
    <message>
        <location filename="../sources/pdfmodel.cpp" line="1074"/>
        <source>Backend:</source>
        <translation type="unfinished">Sistemin arxa tərəfi</translation>
    </message>
</context>
<context>
    <name>qpdfview::PluginHandler</name>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="307"/>
        <source>Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="312"/>
        <source>Supported formats (%1)</source>
        <translation type="unfinished">Dəstəklənən formatlar (%1)</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="323"/>
        <source>Could not match file type of &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="330"/>
        <source>Critical</source>
        <translation type="unfinished">Kritik</translation>
    </message>
    <message>
        <location filename="../sources/pluginhandler.cpp" line="330"/>
        <source>Could not load plug-in for file type &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::PrintDialog</name>
    <message>
        <location filename="../sources/printdialog.cpp" line="64"/>
        <source>Fit to page:</source>
        <translation type="unfinished">Səhifəyə uyğunlaşdır:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="68"/>
        <source>Page ranges:</source>
        <translation type="unfinished">Səhifə aralığı:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="73"/>
        <source>All pages</source>
        <translation type="unfinished">Bütün səhifələr</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="74"/>
        <source>Even pages</source>
        <translation type="unfinished">Cüt səhifələr</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="75"/>
        <source>Odd pages</source>
        <translation type="unfinished">Tək səhifələr</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="78"/>
        <source>Page set:</source>
        <translation type="unfinished">Səhifələr:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="81"/>
        <source>Single page</source>
        <translation type="unfinished">Bir səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="82"/>
        <source>Two pages</source>
        <translation type="unfinished">İki səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="83"/>
        <source>Four pages</source>
        <translation type="unfinished">Dörd səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="84"/>
        <source>Six pages</source>
        <translation type="unfinished">Altı səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="85"/>
        <source>Nine pages</source>
        <translation type="unfinished">Doqquz səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="86"/>
        <source>Sixteen pages</source>
        <translation type="unfinished">16 səhifə</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="89"/>
        <source>Number-up:</source>
        <translation type="unfinished">Bir rəqəm yuxarı:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="92"/>
        <source>Bottom to top and left to right</source>
        <translation type="unfinished">Aşağıdan yixarı və soldan sağa</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="93"/>
        <source>Bottom to top and right to left</source>
        <translation type="unfinished">Aşağıdan yuxarı və sağdan sola</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="94"/>
        <source>Left to right and bottom to top</source>
        <translation type="unfinished">Soldan sağa və aşağıdan yuxarı</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="95"/>
        <source>Left to right and top to bottom</source>
        <translation type="unfinished">Soldan sağa və yuxarıdan aşağı</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="96"/>
        <source>Right to left and bottom to top</source>
        <translation type="unfinished">Sağdan sola və aşağıdan yuxarı</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="97"/>
        <source>Right to left and top to bottom</source>
        <translation type="unfinished">Sağdan sola və yuxarıdan aşağı</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="98"/>
        <source>Top to bottom and left to right</source>
        <translation type="unfinished">Yuxarıdan aşağı və soldan sağa</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="99"/>
        <source>Top to bottom and right to left</source>
        <translation type="unfinished">Yuxarıdan aşağı və sağdan sola</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="102"/>
        <source>Number-up layout:</source>
        <translation type="unfinished">Nömrələr-üstə düzümü:</translation>
    </message>
    <message>
        <location filename="../sources/printdialog.cpp" line="106"/>
        <source>Extended options</source>
        <translation type="unfinished">Geniş seçənəklər</translation>
    </message>
</context>
<context>
    <name>qpdfview::PsSettingsWidget</name>
    <message>
        <location filename="../sources/psmodel.cpp" line="262"/>
        <source>Graphics antialias bits:</source>
        <translation type="unfinished">Qrafikanın bitlərinin hamarlanması:</translation>
    </message>
    <message>
        <location filename="../sources/psmodel.cpp" line="270"/>
        <source>Text antialias bits:</source>
        <translation type="unfinished">Mətn bitlərinin hamarlanması:</translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyClosedMenu</name>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="32"/>
        <source>&amp;Recently closed</source>
        <translation type="unfinished">Ən son bağlanmışlar</translation>
    </message>
    <message>
        <location filename="../sources/recentlyclosedmenu.cpp" line="39"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::RecentlyUsedMenu</name>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="32"/>
        <source>Recently &amp;used</source>
        <translation type="unfinished">Son istifadə edilənlər</translation>
    </message>
    <message>
        <location filename="../sources/recentlyusedmenu.cpp" line="41"/>
        <source>&amp;Clear list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchModel</name>
    <message>
        <location filename="../sources/searchmodel.cpp" line="148"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/searchmodel.cpp" line="182"/>
        <source>&lt;b&gt;%1&lt;/b&gt; occurrences on page &lt;b&gt;%2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SearchableMenu</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="181"/>
        <source>Search for &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qpdfview::SettingsDialog</name>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="108"/>
        <source>General</source>
        <translation type="unfinished">Ümumi</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="141"/>
        <source>&amp;Behavior</source>
        <translation type="unfinished">Davranış</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="142"/>
        <source>&amp;Graphics</source>
        <translation type="unfinished">Qrafiklər</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="143"/>
        <source>&amp;Interface</source>
        <translation type="unfinished">&amp;İnterfeys</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="144"/>
        <source>&amp;Shortcuts</source>
        <translation type="unfinished">Qısa yollar</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="145"/>
        <source>&amp;Modifiers</source>
        <translation type="unfinished">Dəyişdiricilər</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="151"/>
        <source>Defaults</source>
        <translation type="unfinished">Susmaya görə</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="154"/>
        <source>Defaults on current tab</source>
        <translation type="unfinished">Bu sekmeni susmaya görə istifadə et</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="160"/>
        <source>Mouse wheel modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="163"/>
        <source>Mouse button modifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="244"/>
        <source>Open URL:</source>
        <translation type="unfinished">URL aç:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="248"/>
        <source>Auto-refresh:</source>
        <translation type="unfinished">Avto-yeniləmə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="252"/>
        <location filename="../sources/settingsdialog.cpp" line="255"/>
        <location filename="../sources/settingsdialog.cpp" line="518"/>
        <location filename="../sources/settingsdialog.cpp" line="549"/>
        <location filename="../sources/settingsdialog.cpp" line="552"/>
        <location filename="../sources/settingsdialog.cpp" line="556"/>
        <location filename="../sources/settingsdialog.cpp" line="559"/>
        <location filename="../sources/settingsdialog.cpp" line="562"/>
        <location filename="../sources/settingsdialog.cpp" line="571"/>
        <source>Effective after restart.</source>
        <translation type="unfinished">Yenidən başlatdıqda təsirlidir.</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="252"/>
        <source>Track recently used:</source>
        <translation type="unfinished">Son istifadə edilmişləri izlə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="255"/>
        <source>Keep recently closed:</source>
        <translation type="unfinished">Sonuncu bağlanmışarı xatırla:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="259"/>
        <source>Restore tabs:</source>
        <translation type="unfinished">Səkmələri bərpa et:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="262"/>
        <source>Restore bookmarks:</source>
        <translation type="unfinished">Əlfəcinləri bərpa et</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="265"/>
        <source>Restore per-file settings:</source>
        <translation type="unfinished">ön-fayl seçənəklərini bərpa et:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source> min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <source>Save database interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="281"/>
        <source>Synchronize presentation:</source>
        <translation type="unfinished">Nümaişin sinxronizasiyası:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="284"/>
        <source>Default</source>
        <translation type="unfinished">Susmaya görə</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="284"/>
        <source>Presentation screen:</source>
        <translation type="unfinished">Nümayiş ekranı:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="295"/>
        <source>Zoom factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source> ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source>None</source>
        <translation type="unfinished">Heç Biri</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="299"/>
        <source>Highlight duration:</source>
        <translation type="unfinished">Sayrışma müddəri:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="302"/>
        <source>Highlight color:</source>
        <translation type="unfinished">Seçilmə rəngi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="305"/>
        <source>Annotation color:</source>
        <translation type="unfinished">Qeydlərin rəngi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="309"/>
        <source>&apos;%1&apos; is replaced by the absolute file path. &apos;%2&apos; resp. &apos;%3&apos; is replaced by line resp. column number.</source>
        <translation type="unfinished">&apos;%1&apos; tam fayl ünvanı ilə əvəz olunacaq. . &apos;%2&apos; resp. &apos;%3&apos; is replaced by line resp. column number.</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="309"/>
        <source>Source editor:</source>
        <translation type="unfinished">Mənbə redaktoru:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="373"/>
        <source>Use tiling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="376"/>
        <source>Keep obsolete pixmaps:</source>
        <translation type="unfinished">Köhnə pixmasp-ı saxla</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="379"/>
        <source>Use device pixel ratio:</source>
        <translation type="unfinished">Cihazın piksel nisbətindən istifadə et:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="389"/>
        <source>Decorate pages:</source>
        <translation type="unfinished">Səhifələri dekorasiyala:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="392"/>
        <source>Decorate links:</source>
        <translation type="unfinished">Linkləri dekorasiyala:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="395"/>
        <source>Decorate form fields:</source>
        <translation type="unfinished">Xanadan dekorasiyala:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="399"/>
        <source>Background color:</source>
        <translation type="unfinished">Arxa fon rəngi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="402"/>
        <source>Paper color:</source>
        <translation type="unfinished">Səhifənin rəngi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="405"/>
        <source>Presentation background color:</source>
        <translation type="unfinished">Təqdimatın fon rəngi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="409"/>
        <source>Pages per row:</source>
        <translation type="unfinished">Sətrdəki səhifələrin sayı:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="413"/>
        <location filename="../sources/settingsdialog.cpp" line="416"/>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="413"/>
        <source>Page spacing:</source>
        <translation type="unfinished">Səhifələr arası məsafə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="416"/>
        <source>Thumbnail spacing:</source>
        <translation type="unfinished">Miniaturlar arası məsafə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source>Thumbnail size:</source>
        <translation type="unfinished">Cəld baxış şəkilinin ölçüsü:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="420"/>
        <source>Fit to viewport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="565"/>
        <source>Document context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="568"/>
        <source>Tab context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="701"/>
        <source>Open in source editor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="810"/>
        <location filename="../sources/settingsdialog.cpp" line="811"/>
        <location filename="../sources/settingsdialog.cpp" line="812"/>
        <location filename="../sources/settingsdialog.cpp" line="813"/>
        <location filename="../sources/settingsdialog.cpp" line="814"/>
        <location filename="../sources/settingsdialog.cpp" line="815"/>
        <location filename="../sources/settingsdialog.cpp" line="816"/>
        <location filename="../sources/settingsdialog.cpp" line="817"/>
        <location filename="../sources/settingsdialog.cpp" line="818"/>
        <location filename="../sources/settingsdialog.cpp" line="819"/>
        <location filename="../sources/settingsdialog.cpp" line="827"/>
        <source>%1 MB</source>
        <translation type="unfinished">%1 MB</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="424"/>
        <source>Cache size:</source>
        <translation type="unfinished">Keşin ölçüsü:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="292"/>
        <source>Minimal scrolling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="427"/>
        <source>Prefetch:</source>
        <translation type="unfinished">Öncədən yükləmə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="430"/>
        <source>Prefetch distance:</source>
        <translation type="unfinished">Öncədən yükəmə məsafəsi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Top</source>
        <translation type="unfinished">Yuxarı</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Bottom</source>
        <translation type="unfinished">Aşağı</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Left</source>
        <translation type="unfinished">Sol</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="529"/>
        <source>Right</source>
        <translation type="unfinished">Sağ</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="528"/>
        <source>Tab position:</source>
        <translation type="unfinished">Səkmə pozisiyası:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>As needed</source>
        <translation type="unfinished">Lazım olduqda</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>Always</source>
        <translation type="unfinished">Həmişə</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="545"/>
        <source>Exit after last tab:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="571"/>
        <source>Scrollable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="574"/>
        <source>Searchable menus:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="698"/>
        <source>Zoom to selection:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="268"/>
        <location filename="../sources/settingsdialog.cpp" line="534"/>
        <source>Never</source>
        <translation type="unfinished">Heç vaxt</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="105"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="533"/>
        <source>Tab visibility:</source>
        <translation type="unfinished">Səkmənin görünməsi:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="538"/>
        <source>Spread tabs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="542"/>
        <source>New tab next to current tab:</source>
        <translation type="unfinished">Yeni səkmə hazırkının dalınca:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="549"/>
        <source>Recently used count:</source>
        <translation type="unfinished">İstifadə sayı:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="552"/>
        <source>Recently closed count:</source>
        <translation type="unfinished">Ən son bağlanmışların sayı:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="518"/>
        <source>Extended search dock:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="578"/>
        <source>Toggle tool and menu bars with fullscreen:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="556"/>
        <source>File tool bar:</source>
        <translation type="unfinished">Fayl alətlər paneli:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="559"/>
        <source>Edit tool bar:</source>
        <translation type="unfinished">Alətlər panelini redaktə et:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="562"/>
        <source>View tool bar:</source>
        <translation type="unfinished">Alətlər panelinə bax:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="582"/>
        <source>Use page label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="585"/>
        <source>Document title as tab title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="589"/>
        <source>Current page in window title:</source>
        <translation type="unfinished">Hazırkı səhifə pəncərənin başlığında:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="592"/>
        <source>Instance name in window title:</source>
        <translation type="unfinished">Pəncərənin başlığında nüsxənin adı:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="288"/>
        <source>Synchronize outline view:</source>
        <translation type="unfinished">Xətlər görüntüsünü sinxrolaşdır:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="596"/>
        <source>Highlight current thumbnail:</source>
        <translation type="unfinished">Hazırkı görüntünü seç%</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="599"/>
        <source>Limit thumbnails to results:</source>
        <translation type="unfinished">Görüntüləri axtarış sayı ilə limitlə:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="521"/>
        <source>Annotation overlay:</source>
        <translation type="unfinished">Annotasiya üzlüyü:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="524"/>
        <source>Form field overlay:</source>
        <translation type="unfinished">Sahə üzlüyündən:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="683"/>
        <source>Zoom:</source>
        <translation type="unfinished">Böyüklük</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="686"/>
        <source>Rotate:</source>
        <translation type="unfinished">Dönüş:</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="689"/>
        <source>Scroll:</source>
        <translation type="unfinished">SkrolI</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="692"/>
        <source>Copy to clipboard:</source>
        <translation type="unfinished">Yaddaşa kopyala</translation>
    </message>
    <message>
        <location filename="../sources/settingsdialog.cpp" line="695"/>
        <source>Add annotation:</source>
        <translation type="unfinished">Qeyd əlavə et:</translation>
    </message>
</context>
<context>
    <name>qpdfview::ShortcutHandler</name>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="148"/>
        <source>Action</source>
        <translation type="unfinished">Davranış</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="151"/>
        <source>Key sequence</source>
        <translation type="unfinished">Açar sırası</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="269"/>
        <source>Skip backward</source>
        <translation type="unfinished">Geri ötür</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="276"/>
        <source>Skip forward</source>
        <translation type="unfinished">İrəli ötür</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="283"/>
        <source>Move up</source>
        <translation type="unfinished">Yuxarı daşı</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="290"/>
        <source>Move down</source>
        <translation type="unfinished">Aşağı daşı</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="297"/>
        <source>Move left</source>
        <translation type="unfinished">Sola daşı</translation>
    </message>
    <message>
        <location filename="../sources/shortcuthandler.cpp" line="304"/>
        <source>Move right</source>
        <translation type="unfinished">Sağa daşı</translation>
    </message>
</context>
<context>
    <name>qpdfview::TreeView</name>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="632"/>
        <source>&amp;Expand all</source>
        <translation type="unfinished">Hamısını aç</translation>
    </message>
    <message>
        <location filename="../sources/miscellaneous.cpp" line="633"/>
        <source>&amp;Collapse all</source>
        <translation type="unfinished">Hamısını bağla</translation>
    </message>
</context>
</TS>
